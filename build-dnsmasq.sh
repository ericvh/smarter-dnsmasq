#!/bin/bash

docker buildx create --name mybuilder --use

docker buildx build --platform linux/amd64,linux/arm64,linux/arm/v6 -t registry.gitlab.com/arm-research/smarter/smarter-dnsmasq/smarter-dnsmasq:v0.5.1 . --push
