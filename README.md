# smarter-dnsmasq

The repository has the Dockerfile for building the dnsmasq container image used by the smarter-cni CNI plugin for k8s/k3s.

The example instructions below use the docker buildx plugin to build a multi-architecture image that can be used on 32-bit and 64-bit Arm as well as amd64 platforms.

```docker buildx create --name mybuilder --use``` 

```docker buildx build --platform linux/amd64,linux/arm64,linux/arm/v6 -t registry.gitlab.com/arm-research/smarter/smarter-dnsmasq/smarter-dnsmasq:v0.5.1 . --push```
